package gohttprouter

import (
	"embed"
	"net/http"
	"net/url"
	"testing"
)

//go:embed README.md LICENSE.txt
var staticFiles embed.FS

type testWriter struct {
	header     http.Header
	written    []byte
	statusCode int
}

func (tw *testWriter) Header() http.Header {
	if tw.header == nil {
		tw.header = make(http.Header)
	}
	return tw.header
}

func (tw *testWriter) Write(bytes []byte) (int, error) {
	tw.written = append(tw.written, bytes...)
	return len(bytes), nil
}

func (tw *testWriter) WriteHeader(statusCode int) {
	tw.statusCode = statusCode
}

func TestFSReadmeFile(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodGet,
		URL: &url.URL{
			Path: "/README.md",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusOK {
		t.Error("received invalid http status code", tw.statusCode)
	}
}

func TestFSReadmeFileLocal(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)
	fs.UseLocalFolder = true
	fs.LocalFolderPrefix = "/"

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodGet,
		URL: &url.URL{
			Path: "/README.md",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusOK {
		t.Error("received invalid http status code", tw.statusCode)
	}
}

func TestFSReadmeFileHead(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodHead,
		URL: &url.URL{
			Path: "/README.md",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusOK {
		t.Error("received invalid http status code", tw.statusCode)
	}
}

func TestFSLicense(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodHead,
		URL: &url.URL{
			Path: "/LICENSE.txt",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusOK {
		t.Error("received invalid http status code", tw.statusCode)
	}
}

func TestFSFolder(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodHead,
		URL: &url.URL{
			Path: "/",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusNotFound {
		t.Error("received invalid http status code", tw.statusCode)
	}
}

func TestFSFolderLocal(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)
	fs.UseLocalFolder = true
	fs.LocalFolderPrefix = "/"

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodHead,
		URL: &url.URL{
			Path: "/",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusNotFound {
		t.Error("received invalid http status code", tw.statusCode)
	}
}

func TestFSFolderIndex(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)
	fs.IndexFileNames = []string{"README.md"}

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodHead,
		URL: &url.URL{
			Path: "/",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusOK {
		t.Error("received invalid http status code", tw.statusCode)
	}
}

func TestFSNotFound(t *testing.T) {
	// create new FS
	fs := NewFS(&staticFiles)

	// serve the request
	tw := &testWriter{}
	tr := &http.Request{
		Method: http.MethodGet,
		URL: &url.URL{
			Path: "/not-exists.txt",
		},
	}
	fs.ServeHTTP(tw, tr, RoutingInfo{})

	// check data
	if tw.statusCode != http.StatusNotFound {
		t.Error("received invalid http status code", tw.statusCode)
	}
}
