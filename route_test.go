package gohttprouter

import (
	"testing"
)

func TestRouteSimple(t *testing.T) {
	route := newRoute()
	route.parsePath("/home/path", true)
	if route.routes["home"].routes["path"].name != "path" {
		t.Error("error during path parsing")
	}

	// check for non-existing path
	r, _, _ := route.parsePath("/home/not/path", false)
	if r != nil {
		t.Error("found route that doesn't exist")
	}
}

func TestRootNodePlaceholder(t *testing.T) {
	route := newRoute()
	route.parsePath("/:id", true)
	route.parsePath("/:id/name", true)
	if route.routes[":"].name != ":" || route.routes[":"].placeholderName != "id" {
		t.Error("error during path parsing")
	}

	// check for first path
	r, _, parameters := route.parsePath("/123", false)
	if r == nil {
		t.Error("route not found")
	}
	if parameters["id"] != "123" {
		t.Error("missing/invalid parameter ID")
	}

	// check for second path
	r, _, parameters = route.parsePath("/123/name", false)
	if r == nil {
		t.Error("route not found")
	}
	if parameters["id"] != "123" {
		t.Error("missing/invalid parameter ID")
	}
}

func TestRoutePlaceholder(t *testing.T) {
	route := newRoute()
	route.parsePath("/user/:id/home/:name", true)
	placeholderElement := route.routes["user"].routes[":"]
	homePathElement := placeholderElement.routes["home"]

	// check some field values
	if homePathElement.name != "home" {
		t.Error("error during route name parsing with placeholder path;", homePathElement.name)
	}
	if placeholderElement.placeholderName != "id" {
		t.Error("error during placeholder name validation;", placeholderElement.placeholderName)
	}

	// check path finder
	foundRoute, _, foundParameters := route.parsePath("/user/123/home/martin", false)
	if foundRoute != homePathElement.routes[":"] {
		t.Error("wrong path element returned form path finding")
	}
	if foundParameters["id"] != "123" || foundParameters["name"] != "martin" {
		t.Error("wrong parameter value parsed")
	}
}

func TestRouteWildcard(t *testing.T) {
	route := newRoute()
	route.parsePath("/static/*", true)

	// check path
	foundRoute, _, foundParameters := route.parsePath("/static/js/my.js", false)
	if foundRoute == nil {
		t.Error("no route found for wildcard path")
	}
	if foundParameters["*"] != "js/my.js" {
		t.Error("wrong parameter value for wildcard parsed")
	}
}
