# goHTTPRouter
[![GoDoc](https://godoc.org/gitlab.com/martinr92/gohttprouter?status.svg)](https://godoc.org/gitlab.com/martinr92/gohttprouter)
[![pipeline status](https://gitlab.com/martinr92/gohttprouter/badges/master/pipeline.svg)](https://gitlab.com/martinr92/gohttprouter/commits/master)
[![coverage report](https://gitlab.com/martinr92/gohttprouter/badges/master/coverage.svg)](https://gitlab.com/martinr92/gohttprouter/commits/master)
[![codecov](https://codecov.io/gl/martinr92/gohttprouter/branch/master/graph/badge.svg)](https://codecov.io/gl/martinr92/gohttprouter)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/martinr92/gohttprouter)](https://goreportcard.com/report/gitlab.com/martinr92/gohttprouter)

goHTTPRouter is a framework used for HTTP request routing.

# Examples
## Simple Routing
Just replace the standard router of golang with this one:
```golang
import httprouter "gitlab.com/martinr92/gohttprouter/v3"
```
```golang
router := httprouter.New()
router.HandleFunc(http.MethodGet, "/home", func(response http.ResponseWriter, request *http.Request, info httprouter.RoutingInfo) {
    response.Write([]byte("Home"))
})
err := http.ListenAndServe("localhost:8080", router)
```

## Routing with placeholder
A path can also contain placeholder (like :id). If then a request is sent to the url "/user/123" the method gets executed and the URL part (in this case "123") is passed as parameter into your handler function.
```golang
import httprouter "gitlab.com/martinr92/gohttprouter/v3"
```
```golang
router := httprouter.New()
router.HandleFunc(http.MethodGet, "/user/:id", handleUserPages)
router.HandleFunc(http.MethodGet, "/user/:id/settings", handleUserPages)
func handleUserPages(response http.ResponseWriter, request *http.Request, info httprouter.RoutingInfo) {
    id := info.Parameters["id"]
    response.Write([]byte(id))
}
err := http.ListenAndServe("localhost:8080", router)
```

## Serve Static Content
Static files (like JavaScript or CSS) can be served automatically (including caching header and MIME type). It uses the `embed.FS` (since go 1.16) to serve static content.
```golang
import httprouter "gitlab.com/martinr92/gohttprouter/v3"

//go:embed files/statc/*
var staticFiles embed.FS
```
```golang
staticFS := httprouter.NewFS(&staticFiles)
router := httprouter.New()
router.Handle(http.MethodGet, "/static/*", staticFS)
```

For development purpose you can enable the local file serving additionally. The framework checks first, if the file exists locally and serves it directly. If not, the file is served from the `embed.FS`. This helps you during local development so you can modify a CSS file without recompiling everything.
```golang
import httprouter "gitlab.com/martinr92/gohttprouter/v3"
```
```golang
staticFS := httprouter.NewFS(&staticFiles)
staticFS.UseLocalFolder = true
staticFS.LocalFolderPrefix = "some/folder" // optional
```

# License
```
Copyright 2018-2021 Martin Riedl

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
